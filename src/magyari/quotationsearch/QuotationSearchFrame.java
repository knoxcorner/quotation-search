package magyari.quotationsearch;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Jacob on 6/15/2015.
 */
public class QuotationSearchFrame extends JFrame
{

    private final static int WIDTH = 800;
    private final static int HEIGHT = 300;


    private JTextArea quotationArea = new JTextArea(10, 50);
    private JTextField quotationAuthorField = new JTextField(20);

    private JTextField authorFirstField = new JTextField(15);
    private JTextField authorLastField = new JTextField(15);
    private JTextField quoteContainsField = new JTextField(15);
    private JButton searchButton = new JButton("Search");

    public QuotationSearchFrame()
    {
        this.setLayout(new BorderLayout());

        quotationArea.setLineWrap(true);

        searchButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(authorFirstField.getText().isEmpty() && authorLastField.getText().isEmpty() && quoteContainsField.getText().isEmpty())
                {
                    JOptionPane.showMessageDialog(null, "You must enter something to search for!", "Quote Searcher", JOptionPane.OK_OPTION);
                    return;
                }

                Quote quote = Quote.findQuote(authorFirstField.getText(), authorLastField.getText(), quoteContainsField.getText());
                if(quote == null)
                {
                    JOptionPane.showMessageDialog(null, "No quote could be found with the given information.", "Quote Searcher", JOptionPane.OK_OPTION);
                }
                else
                {
                    quotationAuthorField.setText(quote.getAuthorFirstName() + " " + quote.getAuthorLastName());
                    quotationArea.setText(quote.getText());
                    authorFirstField.setText("");
                    authorLastField.setText("");
                    quoteContainsField.setText("");
                }
            }
        });

        //West JPanel:
        JPanel westFullPanel = new JPanel();
        westFullPanel.setLayout(new BoxLayout(westFullPanel, BoxLayout.X_AXIS));
        JPanel westPanel = new JPanel();
        westPanel.setLayout(new BoxLayout(westPanel, BoxLayout.Y_AXIS));
        westPanel.add(new JLabel("Author first name:"));
        westPanel.add(authorFirstField);
        westPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        westPanel.add(new JLabel("Author last name:"));
        westPanel.add(authorLastField);
        westPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        westPanel.add(new JLabel("Quote with text:"));
        westPanel.add(quoteContainsField);
        westPanel.add(Box.createRigidArea(new Dimension(0, 50)));
        westPanel.add(searchButton);
        westPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        westFullPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        westFullPanel.add(westPanel);
        westFullPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        westFullPanel.add(new JSeparator(JSeparator.VERTICAL));
        westFullPanel.add(Box.createRigidArea(new Dimension(10, 0)));


        //Center JPanel:
        JPanel centerPanel = new JPanel();
        //centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
        centerPanel.add(new JLabel("Quote:"));
        centerPanel.add(quotationArea);
        JPanel authorPanel = new JPanel();
        authorPanel.add(new JLabel("Author:"));
        authorPanel.add(quotationAuthorField);
        centerPanel.add(authorPanel);

        //Southern JPanel:
        JPanel southPanel = new JPanel();
        southPanel.add(Box.createHorizontalStrut(WIDTH - 100)); //Spacer to push close button right
        JButton closeButton = new JButton("Close");
        closeButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                setVisible(false);
                new ActionSelectFrame();
            }
        });
        southPanel.add(closeButton);

        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.getContentPane().add(southPanel, BorderLayout.SOUTH);
        this.getContentPane().add(westFullPanel, BorderLayout.WEST);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(WIDTH, HEIGHT);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

}
