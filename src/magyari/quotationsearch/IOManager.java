package magyari.quotationsearch;

import javax.swing.*;
import java.io.*;
import java.util.Scanner;

/**
 * Created by Jacob on 6/15/2015.
 */
public class IOManager
{

    private File file = new File("Quotes.txt");

    public IOManager()
    {
        System.out.println("FFF");
        loadQuotes();
    }

    public void loadQuotes()
    {
        if(!file.exists())
        {

            try
            {
                file.createNewFile();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
            return;
        }
        try
        {
            Scanner scanner = new Scanner(new FileReader(file));
            while(scanner.hasNextLine())
            {
                String line = scanner.nextLine();
                String[] parts = line.split(" ");
                String text = line.substring(line.indexOf(" ", line.indexOf(" ") + 1));
                Quote quote = new Quote(parts[0], parts[1], text);
                Quote.addQuoteNoSave(quote);
            }
            scanner.close();

        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Failed to load Quotes.txt: " + e.getMessage());
        }
    }

    public void saveQuotes()
    {
        try
        {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file, false));
            for(Quote quote : Quote.getQuotes())
            {
                bw.write(quote.toString());
                bw.newLine();
            }
            bw.close();
        } catch (IOException e)
        {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Failed to save Quotes.txt: " + e.getMessage());
        }
    }
}
