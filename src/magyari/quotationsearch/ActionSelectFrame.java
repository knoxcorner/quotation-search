package magyari.quotationsearch;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Jacob on 6/15/2015.
 */
public class ActionSelectFrame extends JFrame
{

    public ActionSelectFrame()
    {
        this.setLayout(new BorderLayout());
        this.getContentPane().add(new JLabel("What would you like to do?"), BorderLayout.NORTH);

        JPanel southPanel = new JPanel();
        JButton quitButton = new JButton("Quit");
        quitButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                setVisible(false);
                System.exit(0);
            }
        });
        southPanel.add(quitButton);

        this.getContentPane().add(southPanel, BorderLayout.SOUTH);

        JPanel centerPanel = new JPanel();
        JButton addQuoteButton = new JButton("Add Quote");
        JButton findQuoteButton = new JButton("Find Quote");
        JButton viewAllQuotesButton = new JButton("View All Quotes");
        addQuoteButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                setVisible(false);
                new QuotationAddFrame();
            }
        });
        findQuoteButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                setVisible(false);
                new QuotationSearchFrame();
            }
        });
        viewAllQuotesButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                setVisible(false);
                new AllQuotesFrame();
            }
        });
        centerPanel.setLayout(new GridLayout(3, 1, 0, 10));
        centerPanel.add(addQuoteButton);
        centerPanel.add(findQuoteButton);
        centerPanel.add(viewAllQuotesButton);
        this.getContentPane().add(centerPanel, BorderLayout.CENTER);

        this.getContentPane().add(Box.createHorizontalStrut(10), BorderLayout.EAST);
        this.getContentPane().add(Box.createHorizontalStrut(10), BorderLayout.WEST);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(200, 200);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }


}
