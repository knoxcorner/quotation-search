package magyari.quotationsearch;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Created by Jacob on 6/15/2015.
 */
public class AllQuotesFrame extends JFrame
{

    public AllQuotesFrame()
    {

        List<Quote> allQuotes = Quote.getQuotes();
        String[] stringQuotes = new String[allQuotes.size()];
        for(int i = 0; i < allQuotes.size(); i++)
        {
            stringQuotes[i] = allQuotes.get(i).getAuthorFirstName() + " " + allQuotes.get(i).getAuthorLastName() + ": " + allQuotes.get(i).getText();
        }
        JList<String> quotes = new JList<>(stringQuotes);

        JButton closeButton = new JButton("Close");
        closeButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                setVisible(false);
                new ActionSelectFrame();
            }
        });
        JPanel southPanel = new JPanel();
        southPanel.add(closeButton);


        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(quotes, BorderLayout.CENTER);
        this.getContentPane().add(southPanel, BorderLayout.SOUTH);
        this.getContentPane().add(Box.createVerticalStrut(10), BorderLayout.NORTH);
        this.getContentPane().add(Box.createHorizontalStrut(10), BorderLayout.EAST);
        this.getContentPane().add(Box.createHorizontalStrut(10), BorderLayout.WEST);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(600, 200);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }


}
