package magyari.quotationsearch;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Jacob on 6/15/2015.
 */
public class QuotationAddFrame extends JFrame
{
    private JTextArea quoteArea = new JTextArea();
    private JTextField authorFirstField = new JTextField();
    private JTextField authorLastField = new JTextField();

    public QuotationAddFrame()
    {


        quoteArea.setLineWrap(true);

        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new GridLayout(3, 2, 15, 30));
        JLabel quoteLabel = new JLabel("Quote:");
        quoteLabel.setHorizontalAlignment(JLabel.RIGHT);
        JLabel authorFirstLabel = new JLabel("Author First Name:");
        authorFirstLabel.setHorizontalAlignment(JLabel.RIGHT);
        JLabel authorLastLabel = new JLabel("Author Last Name:");
        authorLastLabel.setHorizontalAlignment(JLabel.RIGHT);
        centerPanel.add(quoteLabel);
        centerPanel.add(quoteArea);
        centerPanel.add(authorFirstLabel);
        centerPanel.add(authorFirstField);
        centerPanel.add(authorLastLabel);
        centerPanel.add(authorLastField);

        JPanel southPanel = new JPanel();
        JButton closeButton = new JButton("Close");
        closeButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                setVisible(false);
                new ActionSelectFrame();
            }
        });
        JButton addButton = new JButton("Add Quote");
        addButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (quoteArea.getText().isEmpty())
                {
                    JOptionPane.showMessageDialog(null, "The quote is empty!", "Quote Adder", JOptionPane.OK_OPTION);
                    return;
                }
                if (authorFirstField.getText().isEmpty())
                {
                    JOptionPane.showMessageDialog(null, "The author's first name is empty! (The last name can remain empty)", "Quote Adder", JOptionPane.OK_OPTION);
                    return;
                }

                Quote quote = new Quote(authorFirstField.getText(), authorLastField.getText(), quoteArea.getText());
                Quote.addQuote(quote);

                JOptionPane.showMessageDialog(null, "The quote has been added.", "Quote Adder", JOptionPane.OK_OPTION);
            }
        });
        southPanel.add(addButton);
        southPanel.add(closeButton);

        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(Box.createVerticalStrut(10), BorderLayout.NORTH);
        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.getContentPane().add(southPanel, BorderLayout.SOUTH);
        this.getContentPane().add(Box.createRigidArea(new Dimension(120, 0)), BorderLayout.EAST);

        this.setSize(600, 400);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);

    }


}
