package magyari.quotationsearch;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jacob on 6/15/2015.
 */
public class Quote
{
    private String authorFirstName;
    private String authorLastName;
    private String text;

    private static List<Quote> quotes = new ArrayList<>();

    public static IOManager ioManager;

    public Quote(String authorFirstName, String authorLastName, String text)
    {
        this.setAuthorFirstName(authorFirstName);
        this.setAuthorLastName(authorLastName);
        this.setText(text);

    }


    public String getAuthorFirstName()
    {
        return authorFirstName;
    }

    public void setAuthorFirstName(String authorFirstName)
    {
        this.authorFirstName = authorFirstName;
    }

    public String getAuthorLastName()
    {
        return authorLastName;
    }

    public void setAuthorLastName(String authorLastName)
    {
        this.authorLastName = authorLastName;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public boolean contains(String search)
    {
        if(search.isEmpty())
        {
            return false;
        }

        if(this.getText().toLowerCase().contains(search.toLowerCase()))
        {
            return true;
        }

        String[] words = search.split(" ");
        boolean foundAllWords = true;
        for(String word : words)
        {
            if(!this.getText().toLowerCase().contains(word.toLowerCase()))
            {
                foundAllWords = false;
                break;
            }
        }

        return foundAllWords;
    }

    public String toString()
    {
        return authorFirstName + " " + authorLastName + " " + text;
    }

    public static Quote findQuote(String authorFirst, String authorLast, String text)
    {
        ArrayList<Quote> potentialQuotes = new ArrayList<>(quotes);

        if(authorFirst != null && !authorFirst.isEmpty())
        {
            for(int i = 0; i < potentialQuotes.size(); i++)
            {
                if (!potentialQuotes.get(i).getAuthorFirstName().equalsIgnoreCase(authorFirst))
                {
                    potentialQuotes.remove(i);
                    i--;
                }
            }
        }
        if(authorLast != null && !authorLast.isEmpty())
        {
            for(int i = 0; i < potentialQuotes.size(); i++)
            {
                if (!potentialQuotes.get(i).getAuthorLastName().equalsIgnoreCase(authorLast))
                {
                    potentialQuotes.remove(i);
                    i--;
                }
            }
        }
        if(text != null && !text.isEmpty())
        {
            for(int i = 0; i < potentialQuotes.size(); i++)
            {
                if(!potentialQuotes.get(i).contains(text))
                {
                    potentialQuotes.remove(i);
                    i--;
                }
            }
        }

        return potentialQuotes.isEmpty() ? null : potentialQuotes.get(0);
    }

    public static void removeQuote(Quote quote)
    {
        quotes.remove(quote);
        ioManager.saveQuotes();
    }

    public static void addQuote(Quote quote)
    {
        quotes.add(quote);
        ioManager.saveQuotes();
    }

    public static void addQuoteNoSave(Quote quote)
    {
        quotes.add(quote);
    }

    public static List<Quote> getQuotes()
    {
        return quotes;
    }
}
